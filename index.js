const express = require('express')
const jwt = require('jsonwebtoken')
const mysql = require('mysql')
const bodyParser = require('body-parser')

const app = express()

app.use(bodyParser.urlencoded({extended : true}))
app.use(bodyParser.json({ limit: '20mb' }))
// app.use(bodyParser.json())

// app.use(express.limit('8mb'))


app.use( (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Headers", "*")
    next()
})


////////////////end import////////////////

////////////////constant////////////////
const secretKey = 'jruguhisejfkg9U3W=90HT-G209V2N9U-34qig9uvenvm2'
const port = 5000
const error = true

////////////////config////////////////
var mysqlConnection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: '024652209',
    database: 'ku_room'
})

mysqlConnection.connect( (err) => {
    if(!err) {
        console.log('connect database succeeded')
    }
    else {
        console.log('connect failed \n Error ' + JSON.stringify(err, undefined, 2))
    }
})

app.listen(port, () => console.log('Server started on port ' + port))

////////////////end of setup////////////////


////////////////function////////////////
const isNull = (data) => {
    return data === null || data === undefined
}
const isNotNull = (data) => {
    return !isNull(data)
}
const queryFunction = {
    function: {
        sqlValue(value, defaultValue='NULL') {
            if(isNull(value)) return defaultValue

            let valueText = `${value}`
            if(typeof value === 'string') valueText = `'${valueText}'`
            return valueText
        },
        typeJoinWord(typeJoin='left') {
            let join = typeJoin
            switch(typeJoin) {
                case 'left':
                    join = 'LEFT JOIN'
                    break;
                case 'right':
                    join = 'RIGHT JOIN'
                    break;
                case 'inner':
                    join = 'INNER JOIN'
                    break;
                case 'outer':
                    join = 'OUTER JOIN'
                    break;
                default:
                    join = 'LEFT JOIN'
            }
            return join
        },
        joinTo(table1, table2, column1, column2, typeJoin='left') {
            let query = ''
            if( !(table1 && table2 && column1, column2) ) return query
            query = query 
            query = query + ' ' + queryFunction.function.typeJoinWord(typeJoin)
            query = query + ' '  + table2
            query = query + ' '  + 'ON'
            query = query + ' '  + `${table1}.${column1} = ${table2}.${column2}`
            return query
        },
        join(tableList, columnList, typeJoinList=[]) {
            query = tableList[0]
            for (let i = 0; tableList[i] && tableList[i+1]; i += 2) {
                let table1 = tableList[i]
                let table2 = tableList[i+1]
                let column1 = columnList[i] ? columnList[i] : 'id'
                let column2 = columnList[i+1] ? columnList[i+1] : 'id'
                let typeJoin = typeJoinList[i/2] ? typeJoinList[i/2] : 'left'
                let queryJoin = queryFunction.function.joinTo(table1, table2, column1, column2, typeJoin)
                query = query + ` ${queryJoin}`
            }
            return query
        },
        select(columnList) {
            let query = ''
            columnList.forEach(column => {
                query = query + ` ${column} ,`
            })
            query = query.substring(0, query.length-1)
            return query
        },
        set(columnList, valueList) {
            let sqlValue = queryFunction.function.sqlValue

            if(columnList.length !== valueList.length) {
                return 'length not equall'
            }

            let query = ''
            for (let i = 0; i < columnList.length; i++) {
                let column = columnList[i]
                let value = valueList[i]
                if(!column || value===undefined || value===null) continue
                query = query + ` ${column} = ${sqlValue(value)} ,`
            }

            if(query === '') return query

            query = query.substring(0, query.length-1)

            return query
        },
        wherePOS(...params) {
            query = ''
            for (let i = 0; params[i] && params[i+1]; i += 2) {
                columnList = params[i]
                valueList = params[i+1]
                let queryWhere = queryFunction.function.whereAnd(columnList, valueList)
                if(queryWhere === '') continue
                query = query + ` ${queryWhere} OR`
            }
            query = query.substring(0, query.length-2)
            return query
        },
        whereSOM(...params) {
            query = ''
            for (let i = 0; params[i] && params[i+1]; i += 2) {
                columnList = params[i]
                valueList = params[i+1]
                let queryWhere = queryFunction.function.whereOr(columnList, valueList)
                if(queryWhere === '') continue
                query = query + ` ${queryWhere} AND`
            }
            query = query.substring(0, query.length-3)
            return query
        },
        whereAnd(columnList, valueList) {
            let sqlValue = queryFunction.function.sqlValue

            if(columnList.length !== valueList.length) {
                return 'length not equall'
            }

            let query = ''
            for (let i = 0; i < columnList.length; i++) {
                let column = columnList[i]
                let value = valueList[i]
                if(!column || value===undefined || value===null) continue
                query = query + ` ${column} = ${sqlValue(value)} AND`
            }

            if(query === '') return query

            query = query.substring(0, query.length-3)
            query = '(' + query + ')'
            return query
        },
        whereOr(columnList, valueList) {
            let sqlValue = queryFunction.function.sqlValue

            if(columnList.length !== valueList.length) {
                return 'length not equall'
            }

            let query = ''
            for (let i = 0; i < columnList.length; i++) {
                let column = columnList[i]
                let value = valueList[i]
                if(!column || value===undefined || value===null) continue
                query = query + ` ${column} = ${sqlValue(value)} OR`
            }
            
            if(query === '') return query
            
            query = query.substring(0, query.length-2)
            query = '(' + query + ')'
            return query
        }
        // whereForm(...condition) {

        // }
    },
    select: {
        SELECT(columnList) {
            let func = queryFunction.function
            return ' SELECT ' + func.select(columnList)
        },
        FROM(columnList) {
            let func = queryFunction.function
            return ' FROM ' + func.select(columnList)
        },
        WHERE(...params) {
            let func = queryFunction.function
            let whereQuery = func.wherePOS(...params)

            if(whereQuery === '') return ''
            
            return ' WHERE ' + func.wherePOS(...params)
        }
    },
    insert: {
        INSERT_INTO(table) {
            return ` INSERT INTO ${table} `
        },
        COLUMN(columnList) {
            let func = queryFunction.function
            let columnQuery = func.select(columnList)
            return ` (${columnQuery}) `
        },
        VALUE(valueList, defaultValue='NULL') {
            let query = ''
            let func = queryFunction.function
            valueList.forEach(value => {
                query = query + ` ${func.sqlValue(value, defaultValue)} ,`
            })
            query = query.substring(0, query.length-1)
            return ` VALUE (${query}) `
        }
    },
    set: {
        UPDATE(table) {
            return ` UPDATE ${table} ` 
        },
        SET(columnList, valueList) {
            let func = queryFunction.function
            setQuery = func.set(columnList, valueList)
            return ` SET ${setQuery} `
        },
        WHERE(...params) {
            return queryFunction.select.WHERE(...params)
        }
    }
}

var verifyTokenMiddleware = (req, res, next) => {
    let token = req.headers['ku-room-token']
    if(token) {
        jwt.verify(token, secretKey, (err, data) => {
            if(!err) {
                req.token = token
                req.tokenData = data
                next()
            }
            else {
                res.json({
                    message: 'token not match',
                    error,
                    err
                })
            }
        })
    }
    else {
        res.json({
            message: 'you have no token',
            error
        })
    }
}

var verifyToken = (req, res) => {
    return new Promise ( async (resolve, reject) => {
            let token = req.headers['ku-room-token']
            if(token) {
                jwt.verify(token, secretKey, (err, data) => {
                    if(!err) {
                        req.token = token
                        req.tokenData = data
                        resolve('successful')
                    }
                    else {
                        console.log(err)
                        res.json({
                            message: 'token not match',
                            error
                        })
                    }
                })
            }
            else {
                res.json({
                    message: 'you have no token',
                    error
                })
            }
        }
    )

    
}

var getUser = (token) => {
    return new Promise ( async (resolve, reject) => {
            jwt.verify(token, secretKey, (err, data) => {
                if(!err) {
                    resolve(data.user) 
                }
                else {
                    console.log(err)
                    resolve(null)
                }
            })
        }
    )
}

var getUserId = (token) => {
    return new Promise ( async (resolve, reject) => {
            jwt.verify(token, secretKey, (err, data) => {
                if(!err) {
                    resolve(data.user.userId) 
                }
                else {
                    console.log(err)
                    resolve(null)
                }
            })
        }
    )
}

var authLogical = (logic1, logicChar, logic2) => {
    if(logicChar === 'and') 
        return logic1 && logic2
    else if (logicChar === 'or') 
        return logic1 || logic2
    else
        return logic1

}

var getTableRoomFeature = (roomTypeId) => {
    switch(roomTypeId) {
        case 0:
            return 'room_not_provide_yet'
            break;
        case 1:
            return 'room_feature_classroom'
            break;
        case 2:
            return 'room_feature_meetingroom'
            break;
        case 3:
            return 'room_feature_restroom'
            break;
        default:
            console.log('dont defind roomTypeId. then I default to class room')
            return 'room_feature_classroom'
    }
}

var getColumnRoomFeature = (roomTypeId) => {
    switch(roomTypeId) {
        case 1:
            return ['airCondition', 'fan', 'projector', 'whiteBoard', 'blackBoard', 'audioSystem', 'computer', 'wifi']
            break;
        case 2:
            return ['airCondition', 'fan', 'projector', 'whiteBoard', 'blackBoard', 'audioSystem', 'computer', 'wifi']
            break;
        case 3:
            return ['airCondition', 'fan', 'refrigerator', 'microwave', 'televistion', 'wifi', 'fitness', 'pool', 'kitchen']
            break;
        default:
            console.log('dont defind roomTypeId. then I default to class room')
            return ['airCondition', 'fan', 'projector', 'whiteBoard', 'blackBoard', 'audioSystem', 'computer', 'wifi']
    }
}

var getValueRoomFeature = (roomTypeId, featureObject) => {
    let {airCondition, fan, projector, whiteBoard, blackBoard, audioSystem, computer, wifi, refrigerator, kitchen, microwave, televistion, fitness, pool} = featureObject
    switch(roomTypeId) {
        case 1:
            return [airCondition, fan, projector, whiteBoard, blackBoard, audioSystem, computer, wifi]
            break;
        case 2:
            return [airCondition, fan, projector, whiteBoard, blackBoard, audioSystem, computer, wifi]
            break;
        case 3:
            return [airCondition, fan, refrigerator, microwave, televistion, wifi, fitness, pool, kitchen]
            break;
        default:
            console.log('dont defind roomTypeId. then I default to class room')
            return [airCondition, fan, projector, whiteBoard, blackBoard, audioSystem, computer, wifi]
    }
}

////////////////auth////////////////
var authQuery = async (req, res, next, roleQueryList, conditionList, conditionId, index=0) => {
    if(!roleQueryList[index]) {
        return res.json({ authFail: false })        
    }

    let userId = await getUserId(req.token)
    let query = roleQueryList[index]
    
    mysqlConnection.query( query, (err, rows, fields) => {
        let logic1 = !err && rows.length
        let logic2 = userId === conditionId
        let logic = authLogical(logic1, conditionList[index], logic2)
        if(logic) {
            return next()
        }
        else {
            return authQuery(req, res, next, roleQueryList, conditionList, conditionId, index+1)
        }
    })

}

var auth = async (req, res, next, roleIdList, conditionList, conditionId) => {
    //nothing => ''
    //and me => 'and'
    //or me => 'or'
    await verifyToken(req, res)

    let user = await getUser(req.token)
    if(!user) {
        res.json({message: 'you have no token', error})
    }

    let {username, password} = user

    let querySelect = queryFunction.select.SELECT(['*'])

    let queryFrom = queryFunction.select.FROM(['user'])
    
    let queryList = []
    
    roleIdList.forEach( roleId => {
        queryList.push(
            querySelect + 
            queryFrom +
            queryFunction.select.WHERE(
                ['username', 'password', 'roleId'],
                [username, password, roleId]
            ) 
       )
    })

    let roleQueryList = queryList

    if (username && password) {

        let queryIdentify = querySelect + queryFrom + 
            queryFunction.select.WHERE(
                ['username', 'password'],
                [username, password]
            ) 

        mysqlConnection.query( queryIdentify, (err, rows, fields) => {
            if(!err && rows.length) {
                return authQuery(req, res, next, roleQueryList, conditionList, conditionId)
            }
            else {
                console.log(err)
                return res.json({ authFail: false })        
            }
        })    

    }
    else {
        return res.json({ authFail: false })
    }
}

var getUserIdByRoomId = (req, res, roomId) => {
    return new Promise ( async (resolve, reject) => {
        
        mysqlConnection.query(`SELECT * FROM room WHERE id = ${roomId}`
            , (err, rows, fields) => {
                if(!err) {
                    resolve(rows[0].userId)
                }
                else {
                    console.log(err)
                    res.json({message:'find userId by roomId error', error})
                    resolve(null)
                }
            }
        )

    })
}

var authRoomAdminProviderSelf = async (req, res, next) => {
    let roomId = req.params.roomId ? 
        req.params.roomId : req.body.roomId
    if(!parseInt(roomId)) {
        res.json({message:'roomId is not int', error})
    }
    let userId = await getUserIdByRoomId(req, res, roomId)
    req.body._userId = req.body.userId
    req.body.userId = userId
    authAdminProviderSelf(req, res, next)
    
}

var authAdminProviderSelf = (req, res, next) => {
    let userId = req.params.userId ? 
        req.params.userId : req.body.userId
    userId = parseInt(userId) ? parseInt(userId) : 1
    auth(req, res, next, [1,2], ['and', ''], userId)
}

var authAdminProvider = (req, res, next) => {
    auth(req, res, next, [1,2], ['', ''], 0)
}

var authAdmin = (req, res, next) => {
    auth(req, res, next, [2], [''], 0)
}

var authProvider = (req, res, next) => {
    auth(req, res, next, [1], [''], 0)
}

////////////////route////////////////

app.post('/test', (req, res) => {
    let {roomId, roomMainPicture} = req.body
    mysqlConnection.query(`UPDATE room SET roomMainPicture = '${roomMainPicture}'
                            WHERE id=${roomId}`
    , (err, rows, fields) => {
        if(!err) {
            res.json({ message: 'update succeed'})
        }
        else {
            console.log(err)
            res.json({ message: 'fuck u', error })        
        }
    })
})

app.get('/test/userId/:userId', authAdminProviderSelf, (req, res) => {
    res.json({
        message: 'welcome'
    })
})

app.post('/post', verifyTokenMiddleware, (req, res) => {
    res.json({
        message: 'post',
        data: req.tokenData
    })
})

app.get('/token/userId', verifyTokenMiddleware, (req, res) => {
    res.json({
        message: 'this is your token data',
        userId: req.tokenData.user.userId
    })
})

app.get('/token/data', verifyTokenMiddleware, (req, res) => {
    res.json({
        message: 'this is your token data',
        data: req.tokenData
    })
})

app.post('/login', (req, res) => {
    let {username, password} = req.body
    let usernameSafe = mysqlConnection.escape(username)
    let passwordSafe = mysqlConnection.escape(password)
    if (username && password) {
        mysqlConnection.query(`SELECT * FROM user 
                            WHERE username = ${usernameSafe} AND password = ${passwordSafe}`
        , (err, rows, fields) => {
            if(!err && rows.length) {
                let user = {
                    userId: rows[0].id,
                    username,
                    password,
                    roleId: rows[0].roleId
                }
                jwt.sign({user}, secretKey, {expiresIn: '8h'}, (err, token) => {
                    if(!err)
                        res.json({
                            message: `login success --> you are ${rows[0].roleId}`,
                            roleId: rows[0].roleId,
                            token
                        })
                    else {
                        res.json({
                            message: 'create token failed',
                            error
                        })
                    }
                })
                
            }
            else {
                res.json({ message: 'invalid usernname or password', error })        
            }
        })
    }
    else {
        res.json({ message: 'no username or password', error })
    }
})

//get all room
app.get('/room/all', (req, res) => {
    let querySelect = queryFunction.select.SELECT(['*'])
    let queryFrom = queryFunction.select.FROM(['room'])
    let query = querySelect + queryFrom

    mysqlConnection.query(query, (err, rows, field) => {
        if(!err){
            res.json({
                message: 'this is your result',
                roomList: rows
            })
        }
        else {
            console.log(err)
            res.json({
                message: 'query error',
                error
            })
        }
    })
})

app.get('/user/:userId/room', authAdminProviderSelf, (req, res) => {
    let body = req.params
    let querySelect = queryFunction.select.SELECT(['*'])
    let queryFrom = queryFunction.select.FROM(['room'])
    let queryWhere = queryFunction.select.WHERE(['userId'], [body.userId])
    let query = querySelect + queryFrom + queryWhere

    mysqlConnection.query(query, (err, rows, field) => {
        if(!err){
            res.json(rows)
        }
        else {
            console.log(err)
        }
    })
})

app.get('/room/roomId/:roomId', (req, res) => {
    let body = req.params
    let {roomId} = body

    let querySelect = queryFunction.select.SELECT(['*'])
    let queryFrom = queryFunction.select.FROM(['room'])
    let queryWhere = queryFunction.select.WHERE(['id'], [roomId])
    let query = querySelect + queryFrom + queryWhere

    mysqlConnection.query(query, (err, rows, field) => {
        if(!err) {

            let row = rows[0]
            if(rows.length > 0) {
                let roomTypeId = row.roomTypeId
                let table = queryFunction.function.join(
                    ['room', getTableRoomFeature(roomTypeId), 'room', 'user'],
                    ['id', 'roomId', 'userId', 'id'],
                    ['inner', 'inner', 'inner']
                )
                querySelect = queryFunction.select.SELECT(['*'])
                queryFrom = queryFunction.select.FROM([table])
                queryWhere = queryFunction.select.WHERE(
                    ['room.id'],
                    [roomId]
                )
                query = querySelect + queryFrom + queryWhere

                mysqlConnection.query(query, (err, rows, field) => {
                    if(!err) {
                        roomList = rows
                        if(roomList.length > 0) {
                            for(i=0 ; i<roomList.length ; i++) {
                                // roomList[i].roomMainPicture = new Buffer( roomList[i].roomMainPicture, 'binary' ).toString('base64');
                                roomList[i].roomMainPicture = roomList[i].roomMainPicture.toString('utf-8')
                            }
                        }
                        res.json({
                            message: 'this is your reult',
                            room: roomList[0]
                        })
                    }
                    else {
                        console.log(err)
                        res.json({message:'query all table error', error})
                    }
                })
            }
            else {
                res.json({
                    message: 'no room',
                    room: row
                })
            }
            
        }
        else {
            console.log(err)
            res.json({message:'query room table error', error})
        }
    })
})

//get all room by all column
app.post('/room', (req, res) => {
    let {roomId, userId, roomTypeId, province} = req.body
    let {username, buildingName, roomName, location} = req.body
    let {campus, faculty} = req.body
    let {airCondition, fan, projector, whiteBoard, blackBoard, audioSystem, computer, wifi, refrigerator, kitchen, microwave, televistion, fitness, pool} = req.body
    let {costPerHourEnd, costPerHourStart, costPerDayEnd, costPerDayStart, capacityEnd, capacityStart} = req.body
    
    if(!roomTypeId) {
        res.json({
            message: 'you dont have roomTypeId',
            error
        })
    }

    let table = queryFunction.function.join(
        ['room', getTableRoomFeature(roomTypeId), 'room', 'user'],
        ['id', 'roomId', 'userId', 'id'],
        ['inner', 'inner', 'inner']
    )

    let querySelect = queryFunction.select.SELECT(['*'])
    let queryFrom = queryFunction.select.FROM([table])
    let queryWhereMain = queryFunction.select.WHERE(
        ['room.id', 'userId', 'roomTypeId', 'province', 'campus', 'faculty'],
        [roomId, userId, roomTypeId, province, campus, faculty]
    )

    let usernameQuery = isNotNull(username) ? ` username LIKE  '%${username}%' AND` : ''
    let buildingNameQuery = isNotNull(buildingName) ? ` buildingName LIKE  '%${buildingName}%' AND` : ''
    let roomNameQuery = isNotNull(roomName) ? ` roomName LIKE  '%${roomName}%' AND` : ''
    let locationQuery = isNotNull(location) ? ` location LIKE  '%${location}%' AND` : ''
    let queryWhereLike = usernameQuery + buildingNameQuery + roomNameQuery + locationQuery
    queryWhereLike = queryWhereLike.substring(0, queryWhereLike.length-3)
    queryWhereLike = queryWhereLike==='' ? '' : ' AND ' + queryWhereLike
    console.log(queryWhereLike)

    let queryWhereFeature = queryFunction.function.wherePOS(
        ['airCondition', 'fan', 'projector', 'whiteBoard', 'blackBoard', 'audioSystem', 'computer', 'wifi', 'refrigerator', 'kitchen', 'microwave', 'televistion', 'fitness', 'pool'],
        [airCondition, fan, projector, whiteBoard, blackBoard, audioSystem, computer, wifi, refrigerator, microwave, kitchen, televistion, fitness, pool]
    )
    queryWhereFeature = queryWhereFeature==='' ? '' : ' AND ' + queryWhereFeature

    let costPerHourEndQuery = isNotNull(costPerHourEnd) ? ` costPerHour <= ${costPerHourEnd} AND` : ''
    let costPerHourStartQuery = isNotNull(costPerHourStart) ? ` costPerHour >= ${costPerHourStart} AND` : ''
    let costPerDayEndQuery = isNotNull(costPerDayEnd) ? ` costPerDay <= ${costPerDayEnd} AND` : ''
    let costPerDayStartQuery = isNotNull(costPerDayStart) ? ` costPerDay >= ${costPerDayStart} AND` : ''
    let capacityEndQuery = isNotNull(capacityEnd) ? ` capacity <= ${capacityEnd} AND` : ''
    let capacityStartQuery = isNotNull(capacityStart) ? ` capacity >= ${capacityStart} AND` : ''
    let queryWhereValue = costPerHourEndQuery + costPerHourStartQuery + 
                            costPerDayEndQuery + costPerDayStartQuery +
                            capacityEndQuery + capacityStartQuery
    
    queryWhereValue = queryWhereValue.substring(0, queryWhereValue.length-3)
    queryWhereValue = queryWhereValue==='' ? '' : ' AND ' + queryWhereValue

    let query = querySelect + queryFrom + queryWhereMain + 
                queryWhereFeature + queryWhereValue + 
                queryWhereLike
    console.log(query)

    // query = query==='' ? '' : query + ' AND' + ' room_picture.isMainPicture = true'

    mysqlConnection.query(query, (err, rows, field) => {
        if(!err){
            roomList = rows
            if(roomList.length > 0) {
                for(i=0 ; i<roomList.length ; i++) {
                    // roomList[i].roomMainPicture = new Buffer( roomList[i].roomMainPicture, 'binary' ).toString('base64');
                    roomList[i].roomMainPicture = roomList[i].roomMainPicture.toString('utf-8')
                }
            }
            // roomList[0].roomMainPicture = roomList[0].roomMainPicture.toString('utf-8')
            res.json({
                message: 'this is your result',
                roomList
            })
        }
        else {
            console.log(err)
            res.json({
                message: 'query error',
                error
            })
        }
    })
})

app.post('/room/delete/roomId/:roomId', authRoomAdminProviderSelf, (req, res) => {
    mysqlConnection.query(`UPDATE room SET roomTypeId = 4 WHERE id = ${req.params.roomId}`, (err, rows, field) => {
        if(!err){
            res.json({message: `delete room id ${req.params.roomId} successful`})
        }
        else {
            console.log(err)
            res.json({
                message: 'delete room error', error
            })
        }
    })
})

// create room
app.post('/room/create', authAdminProvider, (req, res) => {
    let {roomTypeId, buildingName, roomName, province, location, googleMapUrl, costPerHour, costPerDay, capacity, roomMainPicture} = req.body
    let userId = req.body.userId
    //add ! to if condition
    // if( ! (roomTypeId && buildingName && roomName && province && location && costPerHour && costPerDay && capacity) ) {
    //     res.json({message:'value to create not complete', error})
    // }
    if(!roomTypeId) {
        res.json({message: 'dont have roomTypeId', error})
    }

    let insert = queryFunction.insert

    if(req.tokenData.user.roleId !== 2 || !userId) {
        userId = req.tokenData.user.userId
    }
    let queryInsertInto = insert.INSERT_INTO('room')
    let queryColumn = insert.COLUMN([
        'userId',
        'roomTypeId', 'buildingName', 'roomName', 'province', 
        'location', 'googleMapUrl', 'costPerHour', 'costPerDay', 'capacity', 'roomMainPicture'
    ])
    let queryValue = insert.VALUE([
        userId,
        roomTypeId, buildingName, roomName, province, location, googleMapUrl
        , costPerHour, costPerDay, capacity, roomMainPicture
    ])
    let queryRoom = queryInsertInto + queryColumn + queryValue

    mysqlConnection.query( queryRoom, (err, rows, field) => {
        if(!err){

            let roomId = rows.insertId
            let tableRoomFeature = getTableRoomFeature(roomTypeId)
    
            let queryFeatureInsertInto = insert.INSERT_INTO(`${tableRoomFeature}`)
            let featureColumn = getColumnRoomFeature(roomTypeId)
            let queryFeatureColumn = insert.COLUMN([
                'roomId', ...featureColumn
            ])
            let featureValue = getValueRoomFeature(roomTypeId, req.body)
            let queryFeatureValue = insert.VALUE([
                roomId, ...featureValue
            ], '0')
            let queryFeature = queryFeatureInsertInto + queryFeatureColumn + queryFeatureValue

            mysqlConnection.query( queryFeature, (err, rows, field) => {
                if(!err) {
                    res.json({
                        message: `insert room successful`,
                    })        
                }
                else {
                    console.log(err)
                    res.json({message: 'insert room feature error', error})
                }
            })

        }
        else {
            console.log(err)
            res.json({
                message: 'insert room error', 
                queryRoom,
                error
            })
        }
    })
})

// update room
app.post('/room/update', authRoomAdminProviderSelf, (req, res) => {
    let { roomId, roomTypeId, buildingName, roomName, province, location, googleMapUrl, costPerHour, costPerDay, capacity, roomMainPicture} = req.body
    let userId = req.body._userId

    if(!roomId) {
        res.json({message: 'dont have roomId', error})
    }
    if(!roomTypeId) {
        res.json({message: 'dont have roomTypeId', error})
    }

    let set = queryFunction.set
    
    if(req.tokenData.user.roleId !== 2) {
        userId = req.tokenData.user.userId
    }
    let queryUpdate = set.UPDATE('room')
    let querySet = set.SET(
        [
            'userId',
            'roomTypeId', 'buildingName', 'roomName', 'province', 
            'location', 'googleMapUrl', 'costPerHour', 'costPerDay', 'capacity', 'roomMainPicture'
        ],
        [
            userId,
            roomTypeId, buildingName, roomName, province, location, googleMapUrl
            , costPerHour, costPerDay, capacity, roomMainPicture
        ]
    )
    let queryWhere = set.WHERE(
        ['room.id'],
        [roomId]
    )
    let query = queryUpdate + querySet + queryWhere
    console.log(query)
    

    let queryFeatureUpdate = set.UPDATE(getTableRoomFeature(roomTypeId))
    let queryFeatureSet = set.SET(
        getColumnRoomFeature(roomTypeId),
        getValueRoomFeature(roomTypeId, req.body)
    )
    let queryFeatureWhere = set.WHERE(
        ['roomId'],
        [roomId]
    )
    let queryFeature = queryFeatureUpdate + queryFeatureSet + queryFeatureWhere

    mysqlConnection.query( query, (err, rows, field) => {
        if(!err) {
            mysqlConnection.query( queryFeature, (err, rows, field) => {
                if(!err) {
                    res.json({message:'update room successful'})
                }
                else {
                    console.log(err)
                    res.json({message:'update room_feature error', error})
                }
            })
        }
        else {
            console.log(err)
            res.json({message:'update room error', error})
        }
    })

})

app.get('/user/all', authAdmin, (req, res) => {
    let select = queryFunction.select
    let querySelect = select.SELECT(['*'])
    let queryFrom = select.FROM(['user'])
    query = querySelect + queryFrom
    mysqlConnection.query( query, (err, rows, field) => {
        if(!err) {
            res.json({message: 'query successful', userList: rows})
        }
        else {
            console.log(err)
            res.json({message: 'query error', error})
        }
    })
})

app.get('/user/self', authAdminProvider, (req,res) => {
    res.json({message:'this is user detail', user: req.tokenData.user})
})

app.post('/user/search', authAdmin, (req,res) => {
    let {username} = req.body
    let {userId, campus, faculty} = req.body

    let select = queryFunction.select
    let querySelect = select.SELECT(['*'])
    let queryFrom = select.FROM(['user'])
    let queryWhere = select.WHERE(
        ['id','campus', 'faculty'],
        [userId, campus, faculty]
    )

    let usernameQuery = isNotNull(username) ? ` username LIKE  '%${username}%' AND` : ''
    let queryWhereLike = usernameQuery
    queryWhereLike = queryWhereLike.substring(0, queryWhereLike.length-3)
    if(queryWhere === '' ) {
        queryWhereLike = ' WHERE ' + queryWhereLike
    }
    else {
        queryWhereLike = queryWhereLike==='' ? '' : ' AND ' + queryWhereLike
    }

    let queryRoleId = ' AND roleId != 3 '

    let query = querySelect + queryFrom + queryWhere + queryWhereLike + queryRoleId

    mysqlConnection.query( query, (err, rows, field) => {
        if(!err) {
            res.json({
                message: 'query successful', 
                // campusList: {
                //     campus: campus,
                //     facultyList: rows
                // },
                userList: rows
            })
        }
        else {
            console.log(err)
            res.json({message: 'query error', error})
        }
    })

    // res.json({message:'this is your query', query})

})

app.post('/user/delete/userId/:userId', authAdmin, (req, res) => {
    let userId = req.params.userId
    if(!userId) {
        res.json({message:'no userId', error})
    }
    mysqlConnection.query(`UPDATE user SET roleId = 3 WHERE id = ${userId}`, (err, rows, field) => {
        if(!err){
            res.json({message: `delete user id ${userId} successful`})
        }
        else {
            console.log(err)
            res.json({
                message: 'delete user error', error
            })
        }
    })
})

app.post('/user/create', authAdmin, (req, res) => {
    let {roleId, username, password, campus, faculty, telNo, email, facebook, lineId} = req.body

    if(roleId !== 1 && roleId !== 2) {
        res.json({message:'your roleId is wrong value', roleId})
    }

    let insert = queryFunction.insert

    let queryInsertInto = insert.INSERT_INTO('user')
    let queryColumn = insert.COLUMN([
        'roleId', 'username', 'password', 'campus', 'faculty', 'telNo', 'email', 'facebook', 'lineId'
    ])
    let queryValue = insert.VALUE([
        roleId, username, password, campus, faculty, telNo, email, facebook, lineId
    ])
    let queryUser = queryInsertInto + queryColumn + queryValue

    mysqlConnection.query( queryUser, (err, rows, field) => {
        if(!err){
            res.json({message: `insert user successful`})
        }
        else {
            console.log(err)
            res.json({
                message: 'insert user error', error
            })
        }
    })

    // res.json({message:'yeah', queryUser})
})

app.post('/user/update', authAdmin, (req, res) => {
    let {userId, roleId, username, password, campus, faculty, telNo, email, facebook, lineId} = req.body

    if(!userId) {
        userId = req.tokenData.user.userId
    }
    if(roleId !== 1 && roleId !== 2) {
        res.json({message:'your roleId is wrong value', roleId})
    }

    let set = queryFunction.set
    
    let queryUpdate = set.UPDATE('user')
    let querySet = set.SET(
        [
            'roleId'
            , 'username', 'password', 'campus', 'faculty', 'telNo', 'email', 'facebook', 'lineId'
        ],
        [
            roleId
            , username, password, campus, faculty, telNo, email, facebook, lineId
        ]
    )
    let queryWhere = set.WHERE(
        ['user.id'],
        [userId]
    )

    let queryUser = queryUpdate + querySet + queryWhere

    mysqlConnection.query( queryUser, (err, rows, field) => {
        if(!err){
            res.json({message: `update user successful`})
        }
        else {
            console.log(err)
            res.json({
                message: 'update user error', error
            })
        }
    })

})

//default get route
app.get('/default', (req, res) => {
    res.json({text: 'this is default GET route'})
})

//default post route
app.post('/default', (req, res) => {
    res.json({
        text: 'your body is default POST route', 
        body: req.body
    })
})

//get roleId
app.get('/role', verifyTokenMiddleware, async (req, res) => {
    let userId = await getUserId(req.token)
    
    let querySelect = queryFunction.select.SELECT(['*'])
    let queryFrom = queryFunction.select.FROM(['user'])
    let queryWhere = queryFunction.select.WHERE(['id'], [userId])
    let query = querySelect + queryFrom + queryWhere

    mysqlConnection.query(query, (err, rows, field) => {
        if(!err){
            res.json({
                message: 'this is your role Id',
                roleId: rows[0].roleId
            })
        }
        else {
            console.log(err)
            res.json({
                message: 'query error',
                error
            })
        }
    })
})